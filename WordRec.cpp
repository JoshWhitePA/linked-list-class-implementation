/*
File: WordRec.cpp
Student:Joshua White
Instructor:Dr. Daniel S. Spiegel
Class:CSC 136
Purpose: WordRec class implementation, used for handling WordRec Class

Prepared by Dr. Spiegel, edited by Joshua White
*/

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include "WordRec.h"

using namespace std;

/*-------------------------------------------------WordRec-----*
         |  Function: WordRec
         |
         |  Purpose:WordRec Constructor
         |
         |  Parameters:string word (import)-becomes WordRecs string data member
         |	int count(import) - becomes WordRecs int count.
         |
         |  Returns: None
*-------------------------------------------------------------------*/
WordRec::WordRec(string word,int count)
{
  setWord(word);
  setCount(count);
}

//Sets

/*-------------------------------------------------setWord-----*
         |  Function: setWord
         |
         |  Purpose:Sets the WordRec data member word
         |
         |  Parameters: string theWord(import) assigned to private data member
         |
         |  Returns: None
*-------------------------------------------------------------------*/
void WordRec::setWord(string theWord)
{  word=theWord;
}

/*-------------------------------------------------setCount-----*
         |  Function: setCount
         |
         |  Purpose:Sets the WordRec data member count
         |
         |  Parameters:int theCount (import)- sets private count data member
         |
         |  Returns: None
*-------------------------------------------------------------------*/
void WordRec::setCount(int theCount)
{  count=theCount;
}

/*-------------------------------------------------getWord-----*
         |  Function: getWord
         |
         |  Purpose:Returns WordRec data member word
         |
         |  Parameters:None
         |
         |  Returns:Word
*-------------------------------------------------------------------*/
string WordRec::getWord() const
{
  return word;
}

/*-------------------------------------------------getCount-----*
         |  Function: getCount
         |
         |  Purpose:Return WordRec data member count
         |
         |  Parameters:None
         |
         |  Returns:count
*-------------------------------------------------------------------*/
int WordRec::getCount() const
{
  return count;
}

/*-------------------------------------------------operator++-----*
         |  Function: operator++
         |
         |  Purpose:Operator ++ overload(Pre): Increments data member count
         |
         |  Parameters:None
         |
         |  Returns:*this
*-------------------------------------------------------------------*/
WordRec &WordRec::operator++()
{
  setCount(getCount()+1);
  return *this;
}

/*-------------------------------------------------:operator++(int)-----*
         |  Function: :operator++(int)
         |
         |  Purpose:Operator ++ overload(Post): Increments data member count
         |
         |  Parameters:int(import)
         |
         |  Returns:temp
*-------------------------------------------------------------------*/
WordRec WordRec::operator++(int)
{
  WordRec temp=*this;
  setCount(getCount()+1);
  return temp;
}

/*-------------------------------------------------operator--()-----*
         |  Function: operator--()
         |
         |  Purpose:Operator -- overload (Pre): Decrements data member count
         |
         |  Parameters:None
         |
         |  Returns:*this
*-------------------------------------------------------------------*/
WordRec &WordRec::operator--()
{
  setCount(getCount()-1);
  return *this;
}

/*-------------------------------------------------operator--(int)-----*
         |  Function: operator--(int)
         |
         |  Purpose:Operator -- overload (Post): Decrements data member count
         |
         |  Parameters:int
         |
         |  Returns:temp
*-------------------------------------------------------------------*/
WordRec WordRec::operator--(int)
{
  WordRec temp=*this;
  setCount(getCount()-1);
  return temp;
}

/*-------------------------------------------------operator()-----*
         |  Function: operator()
         |
         |  Purpose:Operator () overload: Returns a substring of data member 	
         |	word
         |
         |  Parameters:int number(import) - used to get certain number of chars
         |
         |  Returns:getWord().substr (0,number)
*-------------------------------------------------------------------*/
string WordRec::operator()(int number) const
{
	return getWord().substr (0,number);
}


/*-------------------------------------------------operator<-----*
         |  Function: operator<
         |
         |  Purpose:Operator < overload: Returns whether a WordRec object's 
         |	word is
		 |	alphanumerically less than another WordRec's word
         |
         |  Parameters:const WordRec &right(import)
         |
         |  Returns:getWord()<right.getWord()
*-------------------------------------------------------------------*/
bool WordRec::operator<(const WordRec &right) const
{return (getWord()<right.getWord());}

/*-------------------------------------------------operator<=-----*
         |  Function: operator<=
         |
         |  Purpose:Operator <= overload: Returns whether a WordRec object's 
         |	word is
		 |	alphanumerically less than or equal to another WordRec's word
         |
         |  Parameters:const WordRec &right(import)
         |
         |  Returns:getWord()<=right.getWord()
*-------------------------------------------------------------------*/
bool WordRec::operator<=(const WordRec &right) const
{return (getWord()<=right.getWord());}


/*-------------------------------------------------operator>-----*
         |  Function: operator>
         |
         |  Purpose:Operator> overload: Returns whether WordRec object's word is
		 |	alphanumerically greater than another WordRec's word
		 |	alphanumerically less than or equal to another WordRec's word
         |
         |  Parameters:const WordRec &right(import)
         |
         |  Returns:getWord()>right.getWord()
*-------------------------------------------------------------------*/
bool WordRec::operator>(const WordRec &right) const
{return (getWord()>right.getWord());}

/*-------------------------------------------------operator>=-----*
         |  Function: operator>=
         |
         |  Purpose:Operator >= overload
		 |	Returns whether a WordRec's word is alphanumerically greater than or 		
		 |	equal to another WordRec's word
         |
         |  Parameters:const WordRec &right(import)
         |
         |  Returns:getWord()>=right.getWord()
*-------------------------------------------------------------------*/
bool WordRec::operator>=(const WordRec &right) const
{return (getWord()>=right.getWord());}
  

/*-------------------------------------------------operator==-----*
         |  Function: operator==
         |
         |  Purpose:Operator == overload: Returns whether a WordRec object's 	
         |	word is equal to another WordRec obeject's word
         |
         |  Parameters:const WordRec &right(import)
         |
         |  Returns:getWord()==right.getWord()
*-------------------------------------------------------------------*/
bool WordRec::operator==(const WordRec &right) const
{return(getWord()==right.getWord());}


/*-------------------------------------------------operator!=-----*
         |  Function:operator!=
         |
         |  Purpose:Operator != overload: Returns whether a WordRec object's 	
         |	word is not equal to another WordRec obeject's word
         |
         |  Parameters:const WordRec &right(import)
         |
         |  Returns:getWord()!=right.getWord()
*-------------------------------------------------------------------*/
bool WordRec::operator!=(const WordRec &right) const
{return(getWord()!=right.getWord());}


/*-------------------------------------------------operator<<-----*
         |  Function:operator<<
         |
         |  Purpose:Operator << overload: Prints a WordRec object
         |
         |  Parameters:ostream &out (import),const const WordRec &right(import)
         |
         |  Returns:out
*-------------------------------------------------------------------*/
ostream &operator<<(ostream &out, const WordRec &right)
{
  out<<setw(15)<<right.getWord()<<setw(15)<<right.getCount();
  return out;
}

/*-------------------------------------------------operator<<=-----*
         |  Function:operator<<=
         |
         |  Purpose:Operator <<= overload:Prints a WordRec object's word
         |
         |  Parameters:ostream &stream(import), const WordRec &right(import)
         |
         |  Returns:stream
*-------------------------------------------------------------------*/
ostream &operator<<=(ostream &stream, const WordRec &right)
{
  stream<<setw(15)<<right.getWord();
  return stream;
}


/*-------------------------------------------------operator>>-----*
         |  Function:operator>>
         |
         |  Purpose:Operator >> overload:Inputs file information into a WordRec 
         |	object
         |
         |  Parameters:ifstream &inf(import), WordRec &right(import)
         |
         |  Returns:inf
*-------------------------------------------------------------------*/
ifstream &operator>>(ifstream &inf, WordRec &right)
{
  string nextWord;
  inf>>nextWord;
  right.setWord(nextWord);
  right.setCount(1);
  return inf;
}
