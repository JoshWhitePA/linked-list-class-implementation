/*
File: LinkedList.cpp
Student:Joshua White
Instructor:Dr. Daniel S. Spiegel
Class:CSC 136
Purpose: Linked List class with List Iterator class

Prepared by Dr. Spiegel, edited by Joshua White
*/

#include <assert.h>
#include <iostream>
#include "LinkedList.h"

using namespace std;

/*-------------------------------------------------:LinkedList() -----*
         |  Function::LinkedList() 
         |
         |  Purpose:Construct empty LinkedList
         |
         |  Parameters:None
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
template <typename eltType> LinkedList<eltType>::LinkedList() : head(NULL)
{}

/*-----------------------LinkedList(LinkedList<eltType> &cl) -----*
         |  Function::LinkedList(LinkedList<eltType> &cl)
         |
         |  Purpose: Copy constructor. copy() does the deep copy
         |
         |  Parameters:LinkedList<eltType> &cl (import)
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
template <typename eltType>
LinkedList<eltType>::LinkedList(LinkedList<eltType> &cl)
{head = copy( cl.head );}

/*-------------------------------------------------~LinkedList()-----*
         |  Function::~LinkedList()
         |
         |  Purpose:Free all nodes
         |
         |  Parameters:None
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
template <typename eltType> LinkedList<eltType>::~LinkedList()
{destroy(head);}

// Assignment operator: copy() does the deep copy
/*-------------------------------------------------operator =-----*
         |  Function::operator =
         |
         |  Purpose:Assignment operator: copy() does the deep copy
         |
         |  Parameters:const LinkedList<eltType>& cl(import)
         |
         |  Returns:  *this
*-------------------------------------------------------------------*/
template <typename eltType> 
	LinkedList<eltType>&LinkedList<eltType>::operator =(const LinkedList<eltType>& cl)
{       if (this != &cl)
    {       destroy(head);
      head = copy(cl.head);
    }
  return *this;
}

// **************-----------------------------------*******************

/*-------------------------------------------------orderedInsert-----*
         |  Function::orderedInsert
         |
         |  Purpose:Place elt into the list in alphanumeric order
		 |	If element is a duplicate, do not insert, return pointer to element 
		 |	(allowing handling of duplicate)
		 |	Otherwise, return NULL to signify it was the first copy in the list
         |
         |  Parameters:const eltType &elt(import)-Used to check if item exists 
         |	in list
         |
         |  Returns: NULL if word is not in list, &(ptr->data) if it is
*-------------------------------------------------------------------*/
template <typename eltType> eltType *LinkedList<eltType>::orderedInsert(const eltType &elt)
{ 
	if (empty()){
		assert(head=new node<eltType>(elt,head)); //Head becomes a 
								//new node if list is empty or there is a lower 
		return NULL;			//number in front of the new item in the list
		}
		
	else if (elt <= head->data) {//if elt needs to be inserted before first node
		if (elt == head->data){//checks if equal and returns address of data
			return  &(head->data);
		}
		assert(head=new node<eltType>(elt,head));// makes new node, points head 									 			
		return NULL;							//to item behind it						
	}
	else {
		node<eltType>* trailp = head;    //NULL
		node<eltType>* p = head->next; //head
		while (p != NULL && elt > p->data) {
			trailp = p;
			p = p->next;
		}
		if (p == NULL || elt < p->data){
			assert((trailp->next=new node<eltType>(elt,p)) !=NULL);
			return NULL;
		}
		else {
			return &(p->data);
		}
		return NULL;
	}
	
}

/*-------------------------------------------------find-----*
         |  Function:find
         |
         |  Purpose:Is this element in the linked list? 
		 |	If so, return a pointer to the element type
		 |	Otherwise, return NULL to signify not found
         |
         |  Parameters:const eltType &elt(import)
         |
         |  Returns: NULL if word is not in list, &(p->data) if it is
*-------------------------------------------------------------------*/
template <typename eltType>
eltType *LinkedList<eltType>::find(const eltType &elt)
{
	node<eltType>* p = head; //head

	while ( p != NULL && p->data < elt )
	{
		p = p->next;
	}
	
	if (p != NULL || p->data == elt){
		return &(p->data);
		}
	return NULL;
}


/*-------------------------------------------------empty-----*
         |  Function:empty
         |
         |  Purpose:Checks if list is empty
         |
         |  Parameters:const eltType &elt(import)
         |
         |  Returns: NULL if word is not in list, &(p->data) if it is
*-------------------------------------------------------------------*/
template <typename eltType> inline bool LinkedList<eltType>::empty()
{return (head == NULL);}


// ********************** Must update this function *******************
// *** Make it match the prerequisite that the item will be found ******
// Remove a node in an ordered list
// Pre: Node will be found
/*-------------------------------------------------remove-----*
         |  Function:remove
         |
         |  Purpose:Remove a node in an ordered list
		 |	Pre: Node will be found
         |
         |  Parameters:const eltType &elt(import)- data used to search
         |  for item being deleted
         |
         |  Returns: None
*-------------------------------------------------------------------*/
template <typename eltType> void LinkedList<eltType>::remove(const eltType &elt)
{ 	
	node<eltType>*	p = head;
	node<eltType>*	trailp = NULL;
	while ( p != NULL &&/**/ p->data != elt )
	{	trailp = p;
		p = p->next;
	}
	if (p == head)
		head = head->next;	// elt is first in the LinkedList
	else
		trailp->next = p->next;	// elt is farther down in the LinkedList
	delete p;
}

/*-------------------------------------------------destroy-----*
         |  Function:destroy
         |
         |  Purpose:Remove all nodes in the linked list, starting at l
         |
         |  Parameters:const node<eltType> *l(import/export)
         |
         |  Returns: None
*-------------------------------------------------------------------*/
template <typename eltType> void LinkedList<eltType>::destroy(node<eltType> *l)
{       while (l != NULL)
    {       node<eltType> *doomed = l;
      l = l->next;
      delete doomed;
    }
}

/*-------------------------------------------------copy-----*
         |  Function:copy
         |
         |  Purpose:The deep copy. Copy the source list l, one node at a time
         |
         |  Parameters:node<eltType> *l(import)
         |
         |  Returns: first
*-------------------------------------------------------------------*/
template <typename eltType>
node<eltType>* LinkedList<eltType>::copy(node<eltType> *l)
{       node<eltType>* first = NULL;    // ptr to beginning of copied LinkedList
  node<eltType>* last = NULL;     // ptr to last item insert in the copy
  if (l != NULL)
    {       assert((first=last=new node<eltType>(l->data,NULL)) != NULL);
      for (node<eltType>* source=l->next;source!=NULL;
	   source=source->next,last=last->next)
	{       last->next = new node<eltType>(source->data,NULL);
	  assert(last->next);
	}
    }
  return first;
}

/*-------------------------------------------------operator<<-----*
         |  Function:operator<<
         |
         |  Purpose:Output a linked list, using a list iterator
         |
         |  Parameters:ostream &os(import), const LinkedList<eltType> &l(import)
         |
         |  Returns: os
*-------------------------------------------------------------------*/
template <typename eltType> ostream& operator<<(ostream &os, const LinkedList<eltType> &l)
{ listItr<eltType> lt(l);
  for (lt.start();lt.more();lt.next())
    os << lt.value() << endl;
  return os;
}

/*-------------------------------------------------countNodes-----*
         |  Function:countNodes
         |
         |  Purpose:Count nodes in a linked list, starting at l
         |
         |  Parameters:node<eltType> *p(import)
         |
         |  Returns: ((p) ?  1+countNodes(p->next) : 0)
*-------------------------------------------------------------------*/
template <typename eltType> int LinkedList<eltType>::countNodes(node<eltType> *p) const
{return ((p) ?  1+countNodes(p->next) : 0);}


/*-------------------------------------------------countNodesInList-----*
         |  Function:countNodesInList
         |
         |  Purpose:Return number of nodes in *this' list
         |
         |  Parameters:none
         |
         |  Returns: countNodes(head)-recursive
*-------------------------------------------------------------------*/
template <typename eltType> int LinkedList<eltType>::countNodesInList() const
{return(countNodes(head));}


/* ****************************************************************
************** List Iterator Implementations *******************
****************************************************************/



/*-------------------------------------------------listItr-----*
         |  Function:listItr
         |
         |  Purpose:a reference to a linked list object
		 |	a pointer to the actual list, initially pointing to its head
         |
         |  Parameters:LinkedList<eltType> &l(import)
         |
         |  Returns:None
*-------------------------------------------------------------------*/
template <typename eltType>
listItr<eltType>::listItr(LinkedList<eltType> &l): itr(l),curr(l.head)
{}

template <typename eltType>
listItr<eltType>::listItr(const LinkedList<eltType> &l) : itr(l),curr(l.head)
{}

/*-------------------------------------------------start-----*
         |  Function:start
         |
         |  Purpose:Set curr to point at itr's head
         |
         |  Parameters:None
         |
         |  Returns:None
*-------------------------------------------------------------------*/
template <typename eltType> void listItr<eltType>::start()
{curr = itr.head;}

/*-------------------------------------------------listItr-----*
         |  Function:listItr
         |
         |  Purpose:Is curr at the end of the list?
         |
         |  Parameters:None
         |
         |  Returns:bool
*-------------------------------------------------------------------*/
template <typename eltType> bool listItr<eltType>::more() const
{return curr != NULL;}

/*-------------------------------------------------next-----*
         |  Function:next
         |
         |  Purpose:Move curr to next node
         |
         |  Parameters:None
         |
         |  Returns:None
*-------------------------------------------------------------------*/
template <typename eltType> void listItr<eltType>::next() 
{assert( curr != NULL );
  curr = curr->next;
}


/*-------------------------------------------------value-----*
         |  Function:value
         |
         |  Purpose:Return data in curr's node. Regardless of assert(), this
		 |	function shouldn't be called until making sure more() returns true
         |
         |  Parameters:None
         |
         |  Returns:curr->data
*-------------------------------------------------------------------*/
template <typename eltType> eltType &listItr<eltType>::value()
{assert( curr != NULL );
  return curr->data;
}


/*-------------------------------------------------value-----*
         |  Function:value
         |
         |  Purpose:Return data in curr's node. Regardless of assert(), this
		 |	function shouldn't be called until making sure more() returns true
         |
         |  Parameters:None
         |
         |  Returns:curr->data
*-------------------------------------------------------------------*/
template <typename eltType> const eltType &listItr<eltType>::value() const
{assert( curr != NULL );
  return curr->data;
}
