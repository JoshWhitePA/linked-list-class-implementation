/*
File: LinkedList.tpp
Student:Joshua White
Instructor:Dr. Daniel S. Spiegel
Class:CSC 136
Purpose: The Tpp file for Linked List

Prepared by Dr. Spiegel
*/
#include "WordRec.h"

template class node <WordRec>;
template class LinkedList <WordRec>;
template class listItr <WordRec>;
template ostream& operator<<(ostream &os, const LinkedList<WordRec> &l);
