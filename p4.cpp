/*
File: p4.cpp
Student:Joshua White
Instructor:Dr. Daniel S. Spiegel
Class:CSC 136
Purpose: Application to test out the functionality of the LinkedList class
Permits user to print in several ways and 
remove words read in from a file and stored with their multiplicity

Prepared by Dr. Spiegel with thanks to Jamie Mason, edited by Joshua White
*/

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include "LinkedList.h"
#include "WordRec.h"

using namespace std;

// Saves a lot of redundant typing.
typedef LinkedList<WordRec> WordList;  

//Function prototype of function that opens the file
bool openFile(ifstream &inf);

//Function prototype of function that displays the prompt asking for a character 
char menu();

//Function prototype of function that displays the prompt asking for a number
int numberPrompt();

//Function prototype of function that displays the prompt asking for a string
string stringPrompt();

//Function prototype of function that searches for a word from the file that
//is the same as the one the user input
void find(const WordList &TheWords);

// Output the first n characters of each word
void substring(const WordList &TheWords);
       
//Print the words from the file with the passed in multiplicity
void mult(const WordList &TheWords);

//For number<--n.m, print the first m characters of words that appeared n times
void multMchars(const WordList &TheWords);

// Remove occurrences of a word in the list
void removeWord(WordList &TheWords);

//Function prototype of function that regulates the output based on user input
void regulateOutput(WordList &TheWords);

//Inputs file into the WordRecs
ifstream &operator>>(ifstream &inf, LinkedList<WordRec> &right);
//void readFile(ifstream &inf, LinkedList<WordRec> TheWords, WordRec wordsA[]);

int main()
{
  //Variable that contains the data from the file
  ifstream inf;
  //Variable that holds the character the the user input
  char character;
  //Variable that contains a true/false value pertaining to whether the 
  //function was opened or not
  bool result;
  //Instantiate a WordList object
  LinkedList<WordRec> TheWords;
  //Open File
  result=openFile(inf);
  
 // WordRec wordsA[1000];
  
  //Output if the file does not exist
  if(!result)
    {
      cout<<"Error:File does not exist.\n";
      return 0;
    }
  //Input file information into the WordList
  	inf >> TheWords;
  //Regulate Output
  regulateOutput(TheWords);
  //Close the file
  inf.close();
  return 0;
}

//Functions

/*-------------------------------------------------openFile-----*
         |  Function: openFile
         |
         |  Purpose:Opens the file
         |
         |  Parameters:ifstream &inf(import/export)
         |
         |  Returns: inf
*-------------------------------------------------------------------*/
bool openFile(ifstream &inf)
{ //Variable to hold the file name
  string filename;
  //Ask for File Name
  cout<<"Enter the data file name >";
  cin>>filename;
  inf.open(filename.c_str());
  return(inf);
}

/*-------------------------------------------------menu-----*
         |  Function: menu
         |
         |  Purpose:Prompt asking for a character
         |
         |  Parameters:None
         |
         |  Returns: SelectedCharacter - Used for the function regulateOutput
*-------------------------------------------------------------------*/
char menu()
{
  char SelectedCharacter;
  cout<<"\n";
  cout<<"A)ll Words in File with their Multiplicity."<<endl;
  cout<<"P)rint all Words Appearing N Times"<<endl;
  cout<<"S)ubstring: First N letters of Each Word"<<endl;
  cout<<"F)ind a Word "<<endl;
  cout<<"M)Characters of Words Appearing N Times"<< endl;
  cout<<"R)emove a Word"<< endl;
  cout<<"Q)uit"<<endl;
  cin>>SelectedCharacter;
  return SelectedCharacter;
}

/*-------------------------------------------------numberPrompt-----*
         |  Function: numberPrompt
         |
         |  Purpose:Prompt asking for a number
         |
         |  Parameters:None
         |
         |  Returns: numberValue - user input
*-------------------------------------------------------------------*/
int numberPrompt()
{
  int numberValue=0;
  cout<<"\n";
  cout<<"Enter an integer value >";
  cin>>numberValue;
  return numberValue;
}

/*-------------------------------------------------stringPrompt-----*
         |  Function: stringPrompt
         |
         |  Purpose: Prompt asking for a string
         |
         |  Parameters:None
         |
         |  Returns: word - User input
*-------------------------------------------------------------------*/
string stringPrompt()
{ string word;
  cout<<"\n";
  cout<<"Enter a word >" ;
  cin>>word;
  return word;
}

/*-------------------------------------------------find-----*
         |  Function: find
         |
         |  Purpose:Searches for a word from the file using the iterator 
         |	interface in the WordList
         |
         |  Parameters:const WordList &TheWords(import)-passed into iterator 
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
void find(const WordList &TheWords)
{ string findWord=stringPrompt();
  listItr<WordRec> it(TheWords);
  bool found=false;
  cout << endl;
  for (it.start();it.more();it.next())
    { 
      if (it.value().getWord()==findWord)
	{
	  cout<< it.value().getWord() << " appeared " << 
				it.value().getCount() << " times in the file\n";
	  found=true;
	}
    }
    if (!found)
      cout<<"The word " << findWord <<" does not exist in the file." << endl;
}
 

/*-------------------------------------------------substring-----*
         |  Function: substring
         |
         |  Purpose:Outputs substrings
         |
         |  Parameters:const WordList &TheWords(import)-passed into iterator 
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
void substring(const WordList &TheWords)
{ int number=numberPrompt();
  while (number<=0)
    {
      cout<<"\n";
      cout<<"Error: Invalid Entry"<<endl;
      number=numberPrompt();
    }
  listItr<WordRec> it(TheWords);
  cout<<"\n";
  cout<<"Substring:\n\n";
  cout<<setw(15)<<"Words"<<endl;
  for (it.start();it.more();it.next())
    cout<< setw(15) << it.value()(number) << endl;
}
       
/*------------------------------------------------- mult-----*
         |  Function: mult
         |
         |  Purpose:Print the multiplicity of the words from the file
         |
         |  Parameters:const WordList &TheWords(import)-passed into iterator 
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
void mult(const WordList &TheWords)
{ int number=numberPrompt();
  while (number<=0)
    {
      cout<<"\n";
      cout<<"Error: Invalid Entry"<<endl;
      number=numberPrompt();
    }
  listItr<WordRec> it(TheWords);
  bool print=false;
  for (it.start();it.more();it.next())
    {
      if (it.value().getCount()==number)
	{
	  if(!print)
	    {
	      cout<<"\n";
	      cout<<"Words appearing "<<number<<" times:\n\n";
	      cout<<setw(15)<<"Words"<<endl;
	      print=true;
	    }
	  cout<<it.value() << endl;
	}
    }
  if(!print)
    {
      cout<<"\nNo word in the file has this multiplicity.\n";
    }
}
 
/*-------------------------------------------------multMchars-----*
         |  Function:multMchars
         |
         |  Purpose:Print the multiplicity of the words from the file
         |
         |  Parameters:const WordList &TheWords(import)-passed into iterator 
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
void multMchars(const WordList &TheWords)
{ listItr<WordRec> it(TheWords);
  double number;
  cout << "Enter double in form n.m >";
  cin >>  number;
  int mult=int(number),numChars=((number+0.001-mult)*10);
  bool print=false;
  for (it.start();it.more();it.next())
    {
      if (it.value().getCount()==mult)
	{
	  if(!print)
	    {
	      cout<<"\n";
	      cout<<"First "<<numChars<<" characters of words appearing "<<mult<<" times:\n";
	      cout<<setw(15)<<"Words"<<endl;
	      cout<<"\n";
	      print=true;
	    }
	  cout<<setw(15)<<it.value()(numChars) << endl;
	}
    }
  if(!print)
    {
      cout<<"\n";
      cout<<"No word in the file has this multiplicity.";
    }
}

/*-------------------------------------------------findWordInList-----*
         |  Function:findWordInList
         |
         |  Purpose:Find a word in the list. Return how many times it was added, 
         |	0 if not found.
         |
         |  Parameters:const WordList &TheWords(import)-list that is iterated
         |	string word (Import)-holds word being checked
         |
         |  Returns:  bool
*-------------------------------------------------------------------*/
WordRec* findWordInList(const WordList &TheWords,string word)
{ listItr<WordRec> it(TheWords);
  bool flag=false;
  for (it.start();it.more();it.next())
    if (it.value().getWord()==word)
      return(&it.value());		// Return a pointer to the found WordRec
  return(0);				// Return NULL
}

// *****************************************
/*-------------------------------------------------removeWord-----*
         |  Function:removeWord
         |
         |  Purpose:Remove occurrences of a word in the list
         |
         |  Parameters:WordList &TheWords (Import)- used for linkedlist
         |	functions
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
void removeWord(WordList &TheWords)
{ 
string word;
  int numRem=0;
  cout << "\n";
  cout << "Enter the Word to Remove >";
  cin >> word;

  WordRec *rePoint = findWordInList(TheWords,word);

  if (rePoint != NULL){
  	cout << "\nThat word appeared " << rePoint->getCount();
  	cout << "\nEnter the number of words you would like removed: ";
  	cin >> numRem;
  	if (numRem > 0 && numRem <= rePoint->getCount()){
  		rePoint->setCount(rePoint->getCount() - numRem);
  		if (rePoint->getCount() <= 0 ){
  			TheWords.remove(*rePoint);
  		}
  	 }	
  	 else{
  			cout << "\nInvalid number! Words not deleted\n";
  		}
  }
  else {
  	cout << "\nWord not found!";
  }
}

/*------------------------------------------------- regulateOutput-----*
         |  Function:regulateOutput
         |
         |  Purpose:Regulates the output based on user input
         |
         |  Parameters:WordList &TheWords (Import)-Used in function calls
         |
         |  Returns:  None
*-------------------------------------------------------------------*/
void regulateOutput(WordList &TheWords)
{ int number;
  string wordFind;
  char inputCharacter=menu();
  while (inputCharacter!='Q' && inputCharacter!='q')
    {
      switch(inputCharacter)
	{
	case 'a':
    case 'A':
          cout << setw(15) << "Word" << setw(18) << "Appearances\n" << TheWords;
          break;
	case 'p':
    case 'P':
	  mult(TheWords);
      break;
	case 's':
	case 'S':
	  substring(TheWords);
	  break;
    case 'f':
	case 'F':
	  find(TheWords);
      break;
	case 'M':
	case 'm':
	  multMchars(TheWords);
	  break;
	case 'R':
	case 'r':
	   removeWord(TheWords);
		break;
	default:
	  cout<<"\n";
          cout<<"Error: Invalid Entry"<<endl;
          break;
        }
      inputCharacter=menu();
    }
}

/*------------------------------------------------- operator>> -----*
         |  Function:operator>>
         |
         |  Purpose: Inputs file into the WordRecs 
         |	If the word read is present, orderedInsert 
		 |	returns a pointer to its WordRec. 
  		 |	In that case, the word was NOT inserted. We increment the Word's 		
  		 |	counter.
         |
         |  Parameters:ifstream &inf (import/export)-File being read
         |  LinkedList<WordRec> &right (import)-LinkedList,hold data from file
         |
         |  Returns:  inf
*-------------------------------------------------------------------*/
ifstream &operator>>(ifstream &inf, LinkedList<WordRec> &right)
{
    string nextWord;
    while(inf >> nextWord) {
   		WordRec wordsA;
  		wordsA.setWord(nextWord);
  		wordsA.setCount(1);
  		WordRec *rePoint;
  		rePoint = right.orderedInsert(wordsA);

  		if (rePoint != NULL){
  			rePoint->setCount(rePoint->getCount()+1);
  		}
  	}
  return inf;
}

 


